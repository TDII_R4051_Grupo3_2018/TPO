/*******************************************************************************************************************************//**
 *
 * @file		DR_lcd.c
 * @brief		Drivers de LCD LPC1769
 * @date		02-12-16
 * @author		Matías Grüner
 *
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** INCLUDES
 **********************************************************************************************************************************/
#include "DR_lcd.h"

/***********************************************************************************************************************************
 *** DEFINES PRIVADOS AL MODULO
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** MACROS PRIVADAS AL MODULO
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** TIPOS DE DATOS PRIVADOS AL MODULO
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** TABLAS PRIVADAS AL MODULO
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** VARIABLES GLOBALES PUBLICAS
 **********************************************************************************************************************************/
volatile unsigned char Buffer_LCD[16];
volatile unsigned int inxInLCD = 0;
volatile unsigned int inxOutLCD = 0;
volatile unsigned int CantidadColaLCD = 0;
volatile int Demora_LCD = 0;

/***********************************************************************************************************************************
 *** VARIABLES GLOBALES PRIVADAS AL MODULO
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** PROTOTIPO DE FUNCIONES PRIVADAS AL MODULO
 **********************************************************************************************************************************/

 /***********************************************************************************************************************************
 *** FUNCIONES PRIVADAS AL MODULO
 **********************************************************************************************************************************/

 /***********************************************************************************************************************************
 *** FUNCIONES GLOBALES AL MODULO
 **********************************************************************************************************************************/

/**
	\fn  void Inic_LCD( void )
	\brief Inicializacion del lcd
 	\author Matías Grüner
 	\date 2 de dic. de 2016
 	\param void
	\return void
*/
void Inic_LCD (void)
{
	SetPINSEL ( LCD_d4, FUNCION_0 );
	SetPINSEL ( LCD_d5, FUNCION_0 );
	SetPINSEL ( LCD_d6, FUNCION_0 );
	SetPINSEL ( LCD_d7, FUNCION_0 );
	SetPINSEL ( LCD_RS, FUNCION_0 );
	SetPINSEL ( LCD_BF, FUNCION_0 );
	SetPINSEL ( LCD_E, FUNCION_0 );

	SetDIR ( LCD_d4, SALIDA );
	SetDIR ( LCD_d5, SALIDA );
	SetDIR ( LCD_d6, SALIDA );
	SetDIR ( LCD_d7, SALIDA );
	SetDIR ( LCD_RS, SALIDA );
	SetDIR ( LCD_BF, ENTRADA );
	SetDIR ( LCD_E, SALIDA );

	Config_LCD();
}

/**
	\fn  void Config_LCD( void )
	\brief Configuracion del lcd
 	\author Matías Grüner
 	\date 2 de dic. de 2016
 	\param void
	\return void
*/
void Config_LCD (void)
{
	unsigned int i;

	SetPIN( LCD_E, OFF );
	Demora_LCD = 10;	//Demora Inicial.
	while ( Demora_LCD );

	for ( i=0 ; i<3 ; i++ )
	{
		SetPIN ( LCD_d4, ON );			//Configuración 8 bits
		SetPIN ( LCD_d5, ON );
		SetPIN ( LCD_d6, OFF );
		SetPIN ( LCD_d7, OFF );

		SetPIN( LCD_RS, OFF );
		SetPIN( LCD_E, ON );
		SetPIN( LCD_E, OFF );

		Demora_LCD = 2;
		while ( Demora_LCD );
	}

	//Configuración 4 bits
	SetPIN ( LCD_d4, OFF );
	SetPIN ( LCD_d5, ON );
	SetPIN ( LCD_d6, OFF );
	SetPIN ( LCD_d7, OFF );

	SetPIN( LCD_RS, OFF );
	SetPIN( LCD_E, ON );
	SetPIN( LCD_E, OFF );

	Demora_LCD = 1;
	while ( Demora_LCD );

	//A partir de acá el LCD pasa a trabajar en formato de 4 bits
	Push_LCD ( 0x28, LCD_CONTROL );		//DL = 0: 4 bits de datos
										//N = 1: 2 líneas
										//F = 0: 5x7 puntos

	Push_LCD ( 0x08, LCD_CONTROL );		//D = 0: display OFF
										//C = 0: cursor OFF
										//B = 0: blink OFF

	Push_LCD ( 0x01, LCD_CONTROL );		//clear display

	Push_LCD ( 0x06, LCD_CONTROL );		//I/O = 1: incrementa puntero
										//S = 0: no shift display

	//Activo el LCD y listo para usar:
	Push_LCD ( 0x0C, LCD_CONTROL );		//D = 1: display ON
										//C = 0: cursor OFF
										//B = 0: blink OFF
}

unsigned char Push_LCD ( unsigned char dato , unsigned char control )
{
	if (CantidadColaLCD >= TOPE_BUFFER_LCD )
		return -1;

	Buffer_LCD [ inxInLCD ] = ( dato >> 4 ) & 0x0f;
	if ( control == LCD_CONTROL )
		Buffer_LCD [ inxInLCD ] |= 0x80;

	inxInLCD ++;
	inxInLCD %= TOPE_BUFFER_LCD;

	Buffer_LCD [ inxInLCD ] = dato & 0x0f;
	if ( control == LCD_CONTROL )
		Buffer_LCD [ inxInLCD ] |= 0x80;

	CantidadColaLCD += 2;

	inxInLCD ++;
	inxInLCD %= TOPE_BUFFER_LCD;

	return 0;
}

int Pop_LCD ( void )
{
	char dato;

	if ( CantidadColaLCD == 0 )
		return -1;

	dato = Buffer_LCD [ inxOutLCD ] ;

	CantidadColaLCD --;

	inxOutLCD ++;
	inxOutLCD %= TOPE_BUFFER_LCD;

	return dato;
}

void Dato_LCD (void)
{
	int data;

	if ( (data = Pop_LCD ()) == -1 )
		return;

	SetPIN ( LCD_d7, ((unsigned char ) data ) >> 3 & 0x01 );
	SetPIN ( LCD_d6, ((unsigned char ) data ) >> 2 & 0x01 );
	SetPIN ( LCD_d5, ((unsigned char ) data ) >> 1 & 0x01 );
	SetPIN ( LCD_d4, ((unsigned char ) data ) >> 0 & 0x01 );

	if( ((unsigned char ) data ) & 0x80 )
		SetPIN ( LCD_RS, OFF );	//Comando
	else
		SetPIN ( LCD_RS, ON );	//Dato

	SetPIN ( LCD_E, ON );
	SetPIN ( LCD_E, OFF );
}
