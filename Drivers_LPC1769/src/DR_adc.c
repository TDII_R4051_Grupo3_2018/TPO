/*******************************************************************************************************************************//**
 *
 * @file		DR_adc.c
 * @brief		Drivers de ADC LPC1769
 * @date		15-05-16
 * @author		Matías Grüner
 *
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** INCLUDES
 **********************************************************************************************************************************/
#include "DR_adc.h"
#include "semphr.h"

/***********************************************************************************************************************************
 *** DEFINES PRIVADOS AL MODULO
 **********************************************************************************************************************************/
#define N_MUESTRAS_A_PROM 50
/***********************************************************************************************************************************
 *** MACROS PRIVADAS AL MODULO
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** TIPOS DE DATOS PRIVADOS AL MODULO
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** TABLAS PRIVADAS AL MODULO
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** VARIABLES GLOBALES PUBLICAS
 **********************************************************************************************************************************/
uint16_t	Vlr_Mdd[4];
uint8_t Entrada_Actual = 0;

//uint8_t flag_ADC = 0;
xSemaphoreHandle sem_FlagListoADC;

/***********************************************************************************************************************************
 *** VARIABLES GLOBALES PRIVADAS AL MODULO
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** PROTOTIPO DE FUNCIONES PRIVADAS AL MODULO
 **********************************************************************************************************************************/

 /***********************************************************************************************************************************
 *** FUNCIONES PRIVADAS AL MODULO
 **********************************************************************************************************************************/

 /***********************************************************************************************************************************
 *** FUNCIONES GLOBALES AL MODULO
 **********************************************************************************************************************************/
/**
	\fn  void ADC_IRQHandler(void)
	\brief Interrupcion del adc
 	\author Matías Grüner
 	\date 5 de jul. de 2016
 	\param void
	\return void
*/
void ADC_IRQHandler(void)
{
	//Las variables estaticas son inicializadas en 0 automaticamente.
	static uint8_t i;
	static uint16_t Vlr_Mdd_CH0[N_MUESTRAS_A_PROM];
	static uint16_t Vlr_Mdd_CH1[N_MUESTRAS_A_PROM];
	static uint16_t Vlr_Mdd_CH2[N_MUESTRAS_A_PROM];
	static uint16_t Vlr_Mdd_CH4[N_MUESTRAS_A_PROM];

	portBASE_TYPE  HigherPriorityTaskWoken = 0;

	AD0CR &= ~(7 << 24 ); //Pongo al ADC en modo STOP.

	switch (Entrada_Actual)//Configuro el multiplexor en la posicion adecuada.
	{
		case 0:
			if( (0x1 & (AD0DR0 >> 31)) == 1)//Se disparo el Done
			{
				if( (0x1 & (AD0DR0 >> 30)) ==0 )//No se sobreescribio el bit/ no se disparo el Overrun
				{
					Vlr_Mdd_CH0[i] = 0xFFF & (AD0DR0 >> 4);

					AD0CR &= ~( 1 << CHN0 );//Deshabilita el canal actual
					AD0CR |= ( 1 << CHN1 );//Habilita el canal siguiente
//					AD0INTEN |= ( 1 << ADINTEN1 );//Habilito la interrupcion del canal siguiente
//					AD0INTEN &= ~( 1 << ADINTEN0 );//Deshabilito la interrupcion del canal actual
				}
			}
			Entrada_Actual = 1;
			break;

		case 1:
			if( (0x1 & (AD0DR1 >> 31)) == 1)//Se disparo el Done
			{
				if( (0x1 & (AD0DR1 >> 30)) ==0 )//No se sobreescribio el bit/ no se disparo el Overrun
				{
					Vlr_Mdd_CH1[i] = 0xFFF & (AD0DR1 >> 4);

					AD0CR &= ~( 1 << CHN1 );//Deshabilita el canal actual
					AD0CR |= ( 1 << CHN2 );//Habilita el canal siguiente
//					AD0INTEN |= ( 1 << ADINTEN2 );//Habilito la interrupcion del canal siguiente
//					AD0INTEN &= ~( 1 << ADINTEN1 );//Deshabilito la interrupcion del canal actual
				}
			}
			Entrada_Actual = 2;
			break;

		case 2:
			if( (0x1 & (AD0DR2 >> 31)) == 1)//Se disparo el Done
			{
				if( (0x1 & (AD0DR2 >> 30)) ==0 )//No se sobreescribio el bit/ no se disparo el Overrun
				{
					Vlr_Mdd_CH2[i] = 0xFFF & (AD0DR2 >> 4);

					AD0CR &= ~( 1 << CHN2 );//Deshabilita el canal actual
					AD0CR |= ( 1 << CHN4 );//Habilita el canal siguiente
//					AD0INTEN |= ( 1 << ADINTEN3 );//Habilito la interrupcion del canal siguiente
//					AD0INTEN &= ~( 1 << ADINTEN2 );//Deshabilito la interrupcion del canal actual
				}
			}
			Entrada_Actual = 3;
			break;

		case 3:
			if( (0x1 & (AD0DR4 >> 31)) == 1)//Se disparo el Done
			{
				if( (0x1 & (AD0DR4 >> 30)) ==0 )//No se sobreescribio el bit/ no se disparo el Overrun
				{
					Vlr_Mdd_CH4[i] = 0xFFF & (AD0DR4 >> 4);

					AD0CR &= ~( 1 << CHN4 );//Deshabilita el canal actual
					AD0CR |= ( 1 << CHN0 );//Habilita el canal siguiente
//					AD0INTEN |= ( 1 << ADINTEN0 );//Habilito la interrupcion del canal siguiente
//					AD0INTEN &= ~( 1 << ADINTEN3 );//Deshabilito la interrupcion del canal actual
				}
			}
			Entrada_Actual = 0;

			uint32_t aux;

			aux=0;
			for(uint8_t i=0; i<N_MUESTRAS_A_PROM; i++)
				aux += Vlr_Mdd_CH0[i];
			Vlr_Mdd[0] = aux/N_MUESTRAS_A_PROM;

			aux=0;
			for(uint8_t i=0; i<N_MUESTRAS_A_PROM; i++)
				aux += Vlr_Mdd_CH1[i];
			Vlr_Mdd[1] = aux/N_MUESTRAS_A_PROM;

			aux=0;
			for(uint8_t i=0; i<N_MUESTRAS_A_PROM; i++)
				aux += Vlr_Mdd_CH2[i];
			Vlr_Mdd[2] = aux/N_MUESTRAS_A_PROM;

			aux=0;
			for(uint8_t i=0; i<N_MUESTRAS_A_PROM; i++)
				aux += Vlr_Mdd_CH4[i];
			Vlr_Mdd[3] = aux/N_MUESTRAS_A_PROM;

			i++;
			i %= N_MUESTRAS_A_PROM;

			xSemaphoreGiveFromISR(sem_FlagListoADC, HigherPriorityTaskWoken);

			break;
	}

	AD0CR |=  ( 1 << 24 );	//Hago START para iniciar una nueva conversion
//
//	if ( Entrada_Actual == 3 )
//	{
//		Entrada_Actual = 0;
//
//
//		xSemaphoreGiveFromISR(sem_FlagListoADC, HigherPriorityTaskWoken);
////		flag_ADC = 1; //Cambio el flag por el semaforo.
//	}
//	else Entrada_Actual++;


	portEND_SWITCHING_ISR(HigherPriorityTaskWoken);
}

/**
	\fn  void Inic_ADC( void )
	\brief Inicializacion del adc
 	\author Matías Grüner
 	\date 5 de jul. de 2016
 	\param void
	\return void
*/
void Inic_ADC( void )
{
	// ADC

	//Canales de ADC no utilizados declarados como salida para evitar ruido.
	Non_Used_Channels();

	PCONP |= 1 << PCADC ;	// Activo la alimentacion del dispositivo desde el registro PCONP pag 63.

	PCLKSEL0 &= ~( 0x03 << 24 );	// Selecciono el clock del ADC como 25MHz: Pag 57 CLK/4 (100Mhz/4)

	AD0CR |= 0x000FE00;		// Y el divisor como 1, para muestrear a <= 200kHz: Pag 577 CLKDIV bits 8 a 15---> [25Mhz/65(CLKDIV+1)]
	//dem.
	/*****		0x000FE00 está expresado en hexadecimal, quiere decir que 'FE' se corresponde al bit 8 a 15 de AD0CR.
	 *****		En binario ---> 1111 1110
	 *****		En decimal --->	1 + 2 + 4 + 8 + 16 + 32 + 64 = 127
	 *****		Haciendo el cálculo anterior llego como resultado a apróx. 200KHz lo cual es correcto.
	 */

	//AD0CR |= ( 1 << CHN5 ) | ( 1 << BURST ) | ( 1 << PDN );

	//Escribe 0's en donde hay 1's en 7b = 0111 desplazados 24 posiciones.
	//Es decir, escribe 0's en donde hay 1's en:
	// 0000 0111 0000 0000 0000 0000 0000 0000
	AD0CR &= ~(7 << 24 ); //Pongo al ADC en modo STOP.


	//Escribe 1's
	AD0CR |= ( 1 << CHN0 ) | ( 1 << PDN ) | ( 1 << 24 );
	//Habilito canal 2, coloco conversión en modo operativo, comienzo ADC (START).

	AD0INTEN |= ( 1 << ADINTEN0 ) | ( 1 << ADGINTEN );
	AD0INTEN |= ( 1 << ADINTEN1 ) | ( 1 << ADGINTEN );
	AD0INTEN |= ( 1 << ADINTEN2 ) | ( 1 << ADGINTEN );
	AD0INTEN |= ( 1 << ADINTEN3 ) | ( 1 << ADGINTEN );
	AD0INTEN |= ( 1 << ADINTEN4 ) | ( 1 << ADGINTEN );
	ISE_ADC ;						// Activo NVIC

	//Inicializo buffer ADC.
	Vlr_Mdd[0]=0;
	Vlr_Mdd[1]=0;
	Vlr_Mdd[2]=0;
	Vlr_Mdd[3]=0;
}


