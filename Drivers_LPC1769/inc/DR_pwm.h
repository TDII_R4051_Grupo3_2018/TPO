/*
 * @file		DR_PWM.h
 * @brief		Declaracion de registros de PWM
 * @date		04/05/2016
 * @author		Matías Grüner
 */

#ifndef DR_PWM_H_
#define DR_PWM_H_

/***********************************************************************************************************************************
 *** INCLUDES GLOBALES
 **********************************************************************************************************************************/
#include "DR_tipos.h"
#include "DR_pll.h"

/***********************************************************************************************************************************
 *** DEFINES GLOBALES
 **********************************************************************************************************************************/
//!< 0x40018000UL: Registro de interrupción del PWM1:
#define	PWM1	( ( uint32_t  * ) 0x40018000UL )

//-----PWM Interrupt Register (PWM1IR - 0x4001 8000)-----//
#define	PWM1IR 		PWM1[0]

#define	PWMMR0		0
#define	PWMMR1		1
#define	PWMMR2		2
#define	PWMMR3		3
#define	PWMCAP0		4
#define	PWMCAP1		5
#define	PWMMR4		8
#define	PWMMR5		9
#define	PWMMR6		10

//-----PWM Timer Control Register (PWM1TCR 0x4001 8004)-----//
#define PWM1TCR 	PWM1[1]

#define	PWMCE		0 //Counter enable.
#define	PWMCR		1 //Counter reset.
#define	PWME		3 //PWM enable.

//-----Timer Counter PWM1TC (PWMTC 0x4001 8008)-----//
#define PWM1TC		PWM1[2]

//-----Prescale Register (PWM1PR 0x4001 800C)-----//
#define	PWM1PR		PWM1[3]

//-----Prescale Counter (PWM1PC 0x4001 8010)-----//
#define	PWM1PC		PWM1[4]

//-----PWM Match Control Register (PWM1MCR - 0x4001 8014)-----//
#define	PWM1MCR		PWM1[5]

#define PWMMR0I		0
#define	PWMMR0R		1
#define PWMMR0S		2
#define PWMMR1I		3
#define	PWMMR1R		4
#define PWMMR1S		5
#define PWMMR2I		6
#define	PWMMR2R		7
#define PWMMR2S		8
#define PWMMR3I		9
#define	PWMMR3R		10
#define PWMMR3S		11
#define PWMMR4I		12
#define	PWMMR4R		13
#define PWMMR4S		14
#define PWMMR5I		15
#define	PWMMR5R		16
#define PWMMR5S		17
#define PWMMR6I		18
#define	PWMMR6R		19
#define PWMMR6S		20

//-----Match Register 0 (PWM1MR0 0x4001 8018)-----//
#define	PWM1MR0		PWM1[6]

//-----Match Register 1 (PWM1MR1 0x4001 801C)-----//
#define	PWM1MR1		PWM1[7]

//-----Match Register 2 (PWM1MR2 0x4001 8020)-----//
#define	PWM1MR2		PWM1[8]

//-----Match Register 3 (PWM1MR3 0x4001 8024)-----//
#define	PWM1MR3		PWM1[9]

//-----PWM Capture Control Register (PWM1CCR - 0x4001 8028)-----//
#define	PWM1CCR		PWM1[10]

//-----Capture Register 0 (PWM1CR0 0x4001 802C)-----//
#define	PWM1CR0		PWM1[11]

//-----Capture Register 1 (PWM1CR1 0x4001 8030)-----//
#define	PWM1CR1		PWM1[12]

//-----Capture Register 2 (PWM1CR2 0x4001 8034)-----//
#define	PWM1CR2		PWM1[13]

//-----Capture Register 3 (PWM1CR3 0x4001 8036)-----//
#define	PWM1CR3		PWM1[14]

//-----Match Register 4 (PWM1MR4 0x4001 8040)-----//
#define	PWM1MR4		PWM1[15]

//-----Match Register 5 (PWM1MR5 0x4001 8044)-----//
#define	PWM1MR5		PWM1[16]

//-----Match Register 6 (PWM1MR6 0x4001 8048)-----//
#define	PWM1MR6		PWM1[17]

//-----PWM Control Register (PWM1PCR - 0x4001 804C)-----//
#define	PWM1PCR		PWM1[19]

#define PWMENA1		9
#define PWMENA2		10
#define PWMENA3		11
#define PWMENA4		12

//-----PWM Latch Enable Register (PWM1LER - 0x4001 8050)-----//
#define	PWM1LER		PWM1[20]

#define	LER0_EN		0
#define	LER1_EN		1
#define	LER2_EN		2
#define	LER3_EN		3
#define	LER4_EN		4
#define	LER5_EN		5
#define	LER6_EN		6

//-----PWM Count Control Register (PWM1CTCR - 0x4001 8070)-----//
#define	PWM1CTCR	PWM1[18]

/***********************************************************************************************************************************
 *** MACROS GLOBALES
 **********************************************************************************************************************************/
#define PCPWM1_BIT		6	//PWM1 power/clock control bit.
#define PCLK_PWM1_BITS	12	//Peripheral clock selection for PWM1.

/***********************************************************************************************************************************
 *** TIPO DE DATOS GLOBALES
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** VARIABLES GLOBALES
 **********************************************************************************************************************************/


/***********************************************************************************************************************************
 *** PROTOTIPOS DE FUNCIONES GLOBALES
 **********************************************************************************************************************************/
void PWMInic ( void );

#endif /* DR_PWM_H_ */
