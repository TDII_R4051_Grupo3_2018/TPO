/*
 * FW_Serie.h
 *
 *  Created on: 09/07/2013
 *      Author: Pablo
 */

#ifndef FW_SERIE_H_
#define FW_SERIE_H_

#include "DR_tipos.h"
#include "DR_pll.h"
#include "DR_EINT.h"

//#define START_TX()	(PopTx((uint8_t*)&U0THR))
#define START_TX()	(PopTx((uint8_t*)&U1THR))

/////////////////////////////////////////////////////////////////////////////////

	//!< ///////////////////   NVIC   //////////////////////////
	//!< Nested Vectored Interrupt Controller (NVIC)
	//!< 0xE000E100UL : Direccion de inicio de los registros de habilitación (set) de interrupciones en el NVIC:
//	#define		ISER		( ( registro  * ) 0xE000E100UL )
	//!< 0xE000E180UL : Direccion de inicio de los registros de deshabilitacion (clear) de interrupciones en el NVIC:
//	#define		ICER		( ( registro  * ) 0xE000E180UL )

	//!< Registros ISER:
	#define		ISER0		ISER[0]
	#define		ISER1		ISER[1]

	//!< Registros ICER:
	#define		ICER0		ICER[0]
	#define		ICER1		ICER[1]


	//!< ///////////////////   PCONP   //////////////////////////
	//!<  Power Control for Peripherals register (PCONP - 0x400F C0C4) [pag. 62 user manual LPC1769]
	//!< 0x400FC0C4UL : Direccion de inicio del registro de habilitación de dispositivos:
//	#define 	PCONP	(* ( ( registro  * ) 0x400FC0C4UL ))


	//!< ///////////////////   PCLKSEL   //////////////////////////
	//!< Peripheral Clock Selection registers 0 and 1 (PCLKSEL0 -0x400F C1A8 and PCLKSEL1 - 0x400F C1AC) [pag. 56 user manual]
	//!< 0x400FC1A8UL : Direccion de inicio de los registros de seleccion de los CLKs de los dispositivos:
//	#define		PCLKSEL		( ( registro  * ) 0x400FC1A8UL )

	//!< Registros PCLKSEL
	#define		PCLKSEL0	PCLKSEL[0]
	#define		PCLKSEL1	PCLKSEL[1]


	//!< ///////////////////   UART0   //////////////////////////
	//!< 0x4000C000UL : Registro de control de la UART0:
	#define		UART0	( ( __RW uint32_t  * ) 0x4000C000UL )

	//!< Registros de la UART0:
	#define		U0THR		UART0[0]
	#define		U0RBR		UART0[0]
	#define		U0DLL		UART0[0]

	#define		U0DLM		UART0[1]
	#define		U0IER		UART0[1]

	#define		U0IIR		((__R __RW uint32_t *)UART0)[2]
	#define		U0FCR		((__W __RW uint32_t *)UART0)[2]

	#define		U0LCR		UART0[3]
	//!< posición 4 no definida [consultar pag. 300 user manual LPC1769]
	#define		U0LSR		UART0[5]
	//!< posición 6 no definida [consultar pag. 300 user manual LPC1769]
	#define		U0SCR		UART0[7]

	//!< ///////////////////   UART1   //////////////////////////
	//!< 0x40010000UL : Registro de control de la UART1:
	#define		UART1	( ( __RW uint32_t  * ) 0x40010000UL )

	//!< Registros de la UART1:
	#define		U1THR		UART1[0]
	#define		U1RBR		UART1[0]
	#define		U1DLL		UART1[0]

	#define		U1DLM		UART1[1]
	#define		U1IER		UART1[1]

	#define		U1IIR		UART1[2]
	#define		U1FCR		UART1[2]

	#define		U1LCR		UART1[3]
	#define		U1MCR		UART1[4]
	#define		U1LSR		UART1[5]
	#define		U1MSR		UART1[6]
	#define		U1SCR		UART1[7]

	//!< //////////BITS DE ANALISIS para todas las UARTs   ////////////////////
	#define 	IER_RBR		0x01
	#define 	IER_THRE	0x02
	#define 	IER_RLS		0x04

	#define 	IIR_PEND	0x01
	#define 	IIR_RLS		0x06
	#define 	IIR_RDA		0x04
	#define 	IIR_CTI		0x0C
	#define 	IIR_THRE	0x02

	#define 	LSR_RDR		0x01
	#define 	LSR_OE		0x02
	#define 	LSR_PE		0x04
	#define 	LSR_FE		0x08
	#define 	LSR_BI		0x10
	#define 	LSR_THRE	0x20
	#define 	LSR_TEMT	0x40
	#define 	LSR_RXFE	0x80

	//!< macros útiles para UARTs
	#define		U0RDR		(U0LSR & LSR_RDR)
	#define		U0THRE		((U0LSR & LSR_THRE) >>5)
	#define		U1RDR		(U1LSR & LSR_RDR)
	#define		U1THRE		((U1LSR & LSR_THRE) >>5)
////////////////////////////////////////////////////////////////////////////////


void InitUART0 (void);
void InitUART1 (void);

extern uint8_t txStart;

#endif /* FW_SERIE_H_ */
