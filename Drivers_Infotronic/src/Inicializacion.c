
/*******************************************************************************************************************************//**
 *
 * @file		Inicializacion.c
 * @brief		Inicializaciones del Kit de Info II Infotronic
 * @date		25/03/16
 * @author		Matías Grüner
 *
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** INCLUDES
 **********************************************************************************************************************************/
#include "Inicializacion.h"

/***********************************************************************************************************************************
 *** DEFINES PRIVADOS AL MODULO
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** MACROS PRIVADAS AL MODULO
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** TIPOS DE DATOS PRIVADOS AL MODULO
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** TABLAS PRIVADAS AL MODULO
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** VARIABLES GLOBALES PUBLICAS
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** VARIABLES GLOBLES PRIVADAS AL MODULO
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** PROTOTIPO DE FUNCIONES PRIVADAS AL MODULO
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** FUNCIONES PRIVADAS AL MODULO
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** FUNCIONES GLOBALES AL MODULO
 **********************************************************************************************************************************/

/**
 \fn  void Inicializar ( void )
 \brief funcion principal
 \param void
 \return void
 */
void Inicializacion ( void )
{
	//Configuración de pines para PWM ----- Movimiento de servos.
	SetPINSEL(PWM1_SV, FUNCION_1); //Puerto 2, bit 1, función 1---> PWM
	SetPINSEL(PWM1_SH, FUNCION_1); //Puerto 2, bit 2, función 1---> PWM

	//Configuración de pines para GPIO/ADC ----- Selectores MUX Analógico.
//	SetPINSEL (SEL0_ADC,FUNCION_0); //Puerto 2, bit 7, función 0---> GPIO
//	SetPINSEL (SEL1_ADC,FUNCION_0); //Puerto 1, bit 29, función 0---> GPIO
	//Configuración de pines para GPIO/ADC ----- Selectores MUX Analógico.
//	SetPINSEL (INHIBIT,FUNCION_0); //Puerto 4, bit 28, función 0---> GPIO
	//Configuro al pines anteriores como entrada, manejo de MUX analógico.
//	SetDIR (SEL0_ADC,SALIDA); //2,7 ---> pin 9 CN4
//	SetDIR (SEL1_ADC,SALIDA); //1,29 ---> pin 11 CN4
//	SetDIR (INHIBIT,SALIDA);  //4,28 ---> pin 13 CN4

	//Configuración de pines para ADC ----- Canal de lectura MUX (por bornera).
	SetPINSEL(ADC_CH0, FUNCION_1);		// CHN0
	SetPINSEL(ADC_CH1, FUNCION_1);		// CHN1
	SetPINSEL(ADC_CH2, FUNCION_1);		// CHN2
	SetPINSEL(ADC_CH3, FUNCION_1);		// CHN3
	SetPINSEL(ADC_CH4, FUNCION_3);		// CHN4


	//Apago buzzer (ruido molesto).
//	SetDIR(BUZZER, SALIDA);

	//!< LPC1769
	Init_PLL();					//Configuro en primer lugar el PLL:Estabiliza el oscilador interno del micro en 100MHz
	EXT0_Inicializacion();
	InitUART1();
	SysTickInic ( );
	PWMInic();
	Inic_ADC();
//	Inic_LCD();
}

