/*******************************************************************************************************************************//**
 *
 * @file		Infotronic.h
 * @brief		Macros, tipos , prototipos, de la aplicacion
 * @date		25-03-16
 * @author		Matías Grüner
 *
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** MODULO
 **********************************************************************************************************************************/

#ifndef INFOTRONIC_H_
#define INFOTRONIC_H_

/***********************************************************************************************************************************
 *** INCLUDES GLOBALES
 **********************************************************************************************************************************/
#include "DR_tipos.h"
#include "Inicializacion.h"

/***********************************************************************************************************************************
 *** DEFINES GLOBALES
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** MACROS GLOBALES
 **********************************************************************************************************************************/
#define		INHIBIT_0			'E'
#define		INHIBIT_1			'A'
#define		ANTIHORARIO			'I'
#define		ZERO				'Z'
#define		HORARIO				'D'

#define		N_VEC				3
#define		SENS				90

#define		MOV_CONDITION		3
/***********************************
 * PROTOCOLOCO DE RECEPCION DE DATOS
 *
 * 'Primer Caracter - Segundo Caracter'
 *
 * Primer Caracter:
 * 'i'	----->	inhibit
 * 'v'	----->	servomotor vertical
 * 'h'	----->	servomotor horizontal
 *
 * Segundo Caracter:
 * INHIBIT_0			'E'
 * INHIBIT_1			'A'
 * ANTIHORARIO			'I'
 * ZERO					'Z'
 * HORARIO				'D'
 **********************************/

/***********************************************************************************************************************************
 *** TIPO DE DATOS GLOBALES
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** VARIABLES GLOBALES
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** PROTOTIPOS DE FUNCIONES GLOBALES
 **********************************************************************************************************************************/
void movimiento_servos_UI ( void );
void enviar_datos ( uint8_t, uint16_t );
void Filtrado_ADC ( uint8_t );
uint16_t prioridad_mov ( uint16_t, uint16_t );
uint16_t rectaMovimiento ( uint16_t);
uint8_t busqueda_tramaRX ( uint8_t , uint8_t );
uint8_t busqueda_datoRX ( uint8_t );

#endif /* INFOTRONIC_H_ */
