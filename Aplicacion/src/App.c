/*******************************************************************************************************************************//**
 *
 * @file		App.c
 * @brief		Aplicación
 * @date		19-12-16
 * @author		Matías Grüner y Rodrigo Videla
 *
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** INCLUDES
 **********************************************************************************************************************************/
#include "App.h"
//Esto lo comentamos porque parece que ya no hace falta, esta incluido en FreeRTOS.
#include <NXP/crp.h>//esto lo agregue, no se por que todos los otros programas no necesitan agregar esto explicitamente y este si, pero  por ahora hacer esto funciona.
__CRP const unsigned int CRP_WORD = CRP_NO_CRP ;

#include "board.h"
#include "FreeRTOS.h"
#include "FreeRTOSConfig.h"
#include "task.h"
#include "semphr.h"
#include "PR_Serie.h"

//Esto tambien esta en FR_Tipos.h, para que pueda tener acceso a los tipos de datos (unit32_t, etc...)


/***********************************************************************************************************************************
 *** DEFINES PRIVADOS AL MODULO
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** MACROS PRIVADAS AL MODULO
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** TIPOS DE DATOS PRIVADOS AL MODULO
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** TABLAS PRIVADAS AL MODULO
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** VARIABLES GLOBALES PUBLICAS
 **********************************************************************************************************************************/
//static uint8_t flag_RX=0;
//volatile int Demora_TX;

extern uint16_t	Vlr_Mdd[4];
xTaskHandle Handle_task_Principal;

//extern uint8_t flag_ADC;
extern xSemaphoreHandle sem_FlagListoADC;

/***********************************************************************************************************************************
 *** VARIABLES GLOBALES PRIVADAS AL MODULO
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** PROTOTIPO DE FUNCIONES PRIVADAS AL MODULO
 **********************************************************************************************************************************/

 /***********************************************************************************************************************************
 *** FUNCIONES PRIVADAS AL MODULO
 **********************************************************************************************************************************/
static uint16_t posicionActual_v = 1500;
static uint16_t posicionActual_h = 1500;

/*******************************************************************************
 * @fn void rectaMovimiento ( uint16_t difProm)
 * @details comparacion de lectura de sensores
 * @details No Portable
 * @param 	uint16_t sensor: Resultado de la resta de los promedios del valor medido por el sensor (va de 0 a 4095)
 * @return 	uint16_t intensidad de movimiento.
 *******************************************************************************/
uint16_t rectaMovimiento ( uint16_t difProm)
{
	uint16_t recMov = 0;

	float ordenada 	= 1000;
	float pendiente	= 0;
	pendiente = (float)1000/(float)8192;

	recMov = (uint16_t) (pendiente*difProm + ordenada);

	return recMov;
	//lo casteamos porque la cuenta da un float
}


/*******************************************************************************
 * @fn void prioridad_mov ( uint16_t , uint16_t , PWM_servos )
 * @details priorida de movimiento
 * @details No Portable
 * @param 	uint16_t promA: adquiere el valor del promedio de sensores superiores o derechos.
 * @		uint16_t promB: adquiere el valor del promedio de sensores inferiores o izquierdos.
 * @return 	uint16_t : módulo de la diferencia de promA y promB
*******************************************************************************/
uint16_t prioridad_mov ( uint16_t promA, uint16_t promB )//despues cambiamos el nombre a difProm(diferencia primedios)
{
	uint16_t priMov = 0;

	priMov = (4096 + promA - promB);

	return priMov;
	//4096 es el valor mmaximo que admite el sensor al ser iluminado al 100% y es el valor en que desplazamos al rango.
}


 /***********************************************************************************************************************************
 *** FUNCIONES GLOBALES AL MODULO
 **********************************************************************************************************************************/

/*******************************************************************************
 * @fn void maquina_estados ( void )
 * @details Máquina de estados, función donde se plasma el funcionamiento de mi proyecto (seguidor solar).
 * @details No Portable
 * @param 	void
 * @return 	void.
*******************************************************************************/
void maquina_estados (void)
{
	/*******************************************************************
	 * Declaración de variables para lectura de valor analógico de LDR's.
	 * Se les asigna el valor de lectura del LDR correspondiente.
	 * *****************************************************************/

	uint16_t r_ii = 0;
	uint16_t r_si = 0;
	uint16_t r_id = 0;
	uint16_t r_sd = 0;


	r_ii = Vlr_Mdd[0];	//valor medido, a cada posicion del vector se le carga el valor medido por el ADC
	r_si = Vlr_Mdd[1];
	r_id = Vlr_Mdd[2];
	r_sd = Vlr_Mdd[3];

	uint16_t prom_S = (uint16_t)(r_si + r_sd) / 2; //Promedio de los LDR's superiores.
	uint16_t prom_I = (uint16_t)(r_ii + r_id) / 2; //Promedio de los LDR's inferiores.
	uint16_t prom_Izq = (uint16_t)(r_si + r_ii) / 2; //Promedio de los LDR's izquierdos.
	uint16_t prom_Der = (uint16_t)(r_sd + r_id) / 2; //Promedio de los LDR's derechos.



	uint16_t priMov_v = 0;
	priMov_v = prioridad_mov(prom_I, prom_S);

	uint16_t rectMov_v = 0;
	rectMov_v = rectaMovimiento(priMov_v);


	if(rectMov_v < 1500)
	{
		posicionActual_v = posicionActual_v - 5;
	}

	if(rectMov_v > 1500)
	{
		posicionActual_v = posicionActual_v + 5;
	}


	if (posicionActual_v < 500)
		posicionActual_v = 500;
	if (posicionActual_v > 2400)
		posicionActual_v = 2400;

	PWM_On(servo_v, posicionActual_v);





	uint16_t priMov_h = 0;
	priMov_h = prioridad_mov(prom_Der, prom_Izq);

	uint16_t rectMov_h = 0;
	rectMov_h = rectaMovimiento(priMov_h);


	if ( posicionActual_v < 1500 )
	{
		if(rectMov_h < 1500)
		{
			posicionActual_h = posicionActual_h + 5;
		}

		if(rectMov_h > 1500)
		{
			posicionActual_h = posicionActual_h - 5;
		}
	}
	else
	{
		if(rectMov_h < 1500)
		{
			posicionActual_h = posicionActual_h - 5;
		}

		if(rectMov_h > 1500)
		{
			posicionActual_h = posicionActual_h + 5;
		}

	}


	if (posicionActual_h < 500)
		posicionActual_h = 500;
	if (posicionActual_h > 2400)
		posicionActual_h = 2400;

	PWM_On(servo_h, posicionActual_h);

	/*****************************************************************************
	 * Transmisión de trama de lectura ADC a la interfaz gráfica.
	 *****************************************************************************/
	enviar_datos( 'a', r_ii);
	vTaskDelay( 6 / portTICK_RATE_MS);

	enviar_datos( 'b', r_si);
	vTaskDelay( 6 / portTICK_RATE_MS);

	enviar_datos( 'c', r_id);
	vTaskDelay( 6 / portTICK_RATE_MS);

	enviar_datos( 'd', r_sd);
	vTaskDelay( 6 / portTICK_RATE_MS);
}

/*******************************************************************************
 * @fn static void task_Principal ( void )
 * @details Tarea principal, contiene a la maquina de estados.
 * @details No Portable
 * @param 	void
 * @return 	void.
*******************************************************************************/
static void task_Principal(void * a)
{
	PWM_On( servo_v, 1500 );
		vTaskDelay(1000 / portTICK_RATE_MS);
	PWM_On( servo_h, 1500 );
		vTaskDelay(1000 / portTICK_RATE_MS);



//	Secuencia inicial de prueba
	for(uint8_t j=0; j<1; j++)
	{
		for(uint16_t i=500; i<2400; i+=1)
		{
			PWM_On( servo_h, i );
			vTaskDelay(5 / portTICK_RATE_MS);
		}

		for(uint16_t i=2400; i>500; i-=1)
		{
			PWM_On( servo_h, i );
			vTaskDelay(5 / portTICK_RATE_MS);
		}
	}

	for(uint8_t j=0; j<1; j++)
	{
		for(uint16_t i=500; i<2400; i+=1)
		{
			PWM_On( servo_v, i );
			vTaskDelay(5 / portTICK_RATE_MS);
		}
		vTaskDelay(2000 / portTICK_RATE_MS);

		for(uint16_t i=2400; i>500; i-=1)
		{
			PWM_On( servo_v, i );
			vTaskDelay(5 / portTICK_RATE_MS);
		}
		vTaskDelay(2000 / portTICK_RATE_MS);
	}




	while (1) {
		if( xSemaphoreTake(sem_FlagListoADC, portMAX_DELAY) )
		{
			maquina_estados();
			vTaskDelay(25 / portTICK_RATE_MS);
		}
	}
}

/*******************************************************************************
 * @fn static void task_TitilaLED ( void )
 * @details Tarea TitilaLED, hace tititlar led del LPC.
 * @details No Portable
 * @param 	void
 * @return 	void.
*******************************************************************************/
static void task_TitilaLED(void * a)
{
	while (1)
	{
		Board_LED_Toggle(0);
		vTaskDelay(500 / portTICK_RATE_MS);
	}
}


/*******************************************************************************
 * @fn static void task_ComRecibir ( void )
 * @details Tarea Com, Recibe datos desde la interfaz grafica.
 * @details No Portable
 * @param 	void
 * @return 	void.
*******************************************************************************/
static void task_ComRecibir(void * a)
{
	uint8_t dato = 0;
	typedef enum {
		E_AUTO,
		E_REMOTO,
		E_RECIBI_CORCH,
		E_RECIBI_H,
		E_RECIBI_HI,
		E_RECIBI_HZ,
		E_RECIBI_HD,
		E_RECIBI_V,
		E_RECIBI_VI,
		E_RECIBI_VZ,
		E_RECIBI_VD
	}E_EstadoRecepcion;

	static E_EstadoRecepcion estado = E_AUTO;


	while (1)
	{
		if( !PopRx(&dato) )
		{
			switch(estado)
			{
				case E_AUTO:
					if(dato == 'R')
					{
						Board_LED_Set(0, 1);
						vTaskSuspend(Handle_task_Principal);
						estado = E_REMOTO;
					}
					vTaskDelay(10 / portTICK_RATE_MS);
					break;

				case E_REMOTO:
					if(dato == 'A')
					{
						Board_LED_Set(0, 0);
						vTaskResume(Handle_task_Principal);
						estado = E_AUTO;
					}
					else if(dato == '[')
						estado = E_RECIBI_CORCH;

					vTaskDelay(10 / portTICK_RATE_MS);
					break;

				case E_RECIBI_CORCH:
					if(dato == 'h')
						estado = E_RECIBI_H;
					else if(dato == 'v')
						estado = E_RECIBI_V;
					else
						estado = E_REMOTO;
					break;

				case E_RECIBI_H:
					if(dato == 'I')
						posicionActual_h -= 25;

					if(dato == 'Z')
						posicionActual_h = 1500;

					if(dato == 'D')
						posicionActual_h += 25;

					PWM_On(servo_h, posicionActual_h);
					estado = E_REMOTO;
					break;

				case E_RECIBI_V:
					if(dato == 'I')
						posicionActual_v -= 25;

					if(dato == 'Z')
						posicionActual_v = 1500;

					if(dato == 'D')
						posicionActual_v += 25;

					PWM_On(servo_v, posicionActual_v);
					estado = E_REMOTO;
					break;

				default:
					estado = E_REMOTO;
					break;
			}
		}
	}
}


static void initHardware(void)
{
    SystemCoreClockUpdate();

    Board_Init();

    Board_LED_Set(0, false);
}

/**
 \fn  int main(void)
 \brief funcion principal
 \param void
 \return void
 */
int main(void)
{
	initHardware();
	Inicializacion();

	sem_FlagListoADC = xSemaphoreCreateMutex();

	if(sem_FlagListoADC)
	{
//		xTaskCreate(task_TitilaLED, (const char *)"task_TitilaLED", configMINIMAL_STACK_SIZE*2, 0, tskIDLE_PRIORITY+1, 0);
		xTaskCreate(task_Principal, (const char *)"task_Principal", configMINIMAL_STACK_SIZE*2, 0, tskIDLE_PRIORITY+1, &Handle_task_Principal);
		xTaskCreate(task_ComRecibir, (const char *)"task_ComRecibir", configMINIMAL_STACK_SIZE*2, 0, tskIDLE_PRIORITY+1, 0);
		vTaskStartScheduler();
	}

	while ( 1 ){
	}

    return 0 ;
}
