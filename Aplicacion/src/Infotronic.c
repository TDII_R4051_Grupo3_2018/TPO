/*******************************************************************************************************************************//**
 *
 * @file		Infotronic.c
 * @brief		Demostracion de funcionamiento del Kit de Info II
 * @date		25-03-16
 * @author		Matías Grüner
 *
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** INCLUDES
 **********************************************************************************************************************************/
#include "Infotronic.h"
#include "PR_Serie.h"
/***********************************************************************************************************************************
 *** DEFINES PRIVADOS AL MODULO
 **********************************************************************************************************************************/


/***********************************************************************************************************************************
 *** MACROS PRIVADAS AL MODULO
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** TIPOS DE DATOS PRIVADOS AL MODULO
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** TABLAS PRIVADAS AL MODULO
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** VARIABLES GLOBALES PUBLICAS
 **********************************************************************************************************************************/
volatile int Demora_PWM;
extern uint16_t	Vlr_Mdd [3];
volatile uint16_t angle_h = 1500;
volatile uint16_t angle_v = 1500;

/***********************************************************************************************************************************
 *** VARIABLES GLOBALES PRIVADAS AL MODULO
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** PROTOTIPO DE FUNCIONES PRIVADAS AL MODULO
 **********************************************************************************************************************************/

 /***********************************************************************************************************************************
 *** FUNCIONES PRIVADAS AL MODULO
 **********************************************************************************************************************************/

 /***********************************************************************************************************************************
 *** FUNCIONES GLOBALES AL MODULO
 **********************************************************************************************************************************/





/*******************************************************************************
 * @fn void movimiento_servos_UI ( void )
 * @details Movimiento de servos a partir de la interfaz gráfica desarrollada en QT Creator.
 * @details No Portable
 * @param 	uint8_t servo Recibe 'h' o 'v' en base a si es horizontal o vertical
 * @return 	void.
*******************************************************************************/
void movimiento_servos_UI (void)
{
	PWM_servos S;
	uint16_t angle;
	static uint16_t angle_h_UI = 1500;
	static uint16_t angle_v_UI = 1500;

	if ( busqueda_datoRX( 'h' ) == 1 )
		S = servo_h;
	if ( busqueda_datoRX( 'v' ) == 1 )
		S = servo_v;

	if ( S == servo_h )
		angle = angle_h_UI;
	if ( S == servo_v )
		angle = angle_v_UI;

	if ( busqueda_tramaRX( 'h', ANTIHORARIO ) == 1 || busqueda_tramaRX( 'v', ANTIHORARIO ) == 1)
	{
						while ( angle != 1000 )
							{
								Demora_PWM = 1;
								while ( Demora_PWM );
								PWM_On(S, angle);
								angle-=1;
							}
	}
	if ( busqueda_tramaRX( 'h', ZERO ) == 1 || busqueda_tramaRX( 'v', ZERO ) == 1)
	{
						while ( angle != 1500 )
							{
								Demora_PWM = 1;
								while ( Demora_PWM );
								PWM_On(S, angle);
								if ( angle < 1500 )
									angle+=1;
								if ( angle > 1500 )
									angle-=1;
							}
	}
	if ( busqueda_tramaRX( 'h', HORARIO ) == 1 || busqueda_tramaRX( 'v', HORARIO ) == 1)
	{
						while ( angle != 2000 )
							{
								Demora_PWM = 1;
								while ( Demora_PWM );
								PWM_On(S, angle);
								angle+=1;
							}
	}

	if ( S == servo_h )
		angle_h_UI = angle;
	if ( S == servo_v )
		angle_v_UI = angle;
}





/*******************************************************************************
 * @fn void enviar_datos ( uint8_t spos_1, uint8_t spos_2, uint16_t value )
 * @details envía datos de adC a lla UI.
 * @details No Portable
 * @param 	[in] spos_1: indica si es sensor inferior o superior.
 * 			[in] spos_2: indica si es sensor  izquierdo o derecho.
 * 			[in] value: valor de ADC.
 * @return 	void.
*******************************************************************************/
void enviar_datos ( uint8_t ID, uint16_t value )
{
	PushTx('<');
	PushTx(ID);

//	Envia cada char por separado
	PushTx((value%10)+48);
	value/=10;

	PushTx((value%10)+48);
	value/=10;

	PushTx((value%10)+48);
	value/=10;

	PushTx(value+48);

	PushTx('>');
}




///*******************************************************************************
// * @fn void void Filtrado_ADC (uint8_t  )
// * @details filtrado ADC
// * @details No Portable
// * @param	uint8_t y: indice de vector.
// * @return 	void.
//*******************************************************************************/
//void Filtrado_ADC (uint8_t y)
//{
//	static uint16_t sens = 90;
//	uint16_t aux0, aux1, ss;
//
//	/******************************************
//	 * Análisis de trama de RX. Modificaciión
//	 * de sensibilidad???
//	 ******************************************/
//	for ( int i=0 ; i<=MAX_RX ; i++ )
//	{
//		if ( (bufferRX[i] >= 48 && bufferRX[i] <= 57) && (bufferRX[i+1] >= 48 && bufferRX[i+1] <= 57) )
//		{
//			aux0 = bufferRX[i];
//			aux1 = bufferRX[i+1];
//			i = MAX_RX;
//		}
//	}
//
//	//Guardo dato recibido en auxiliar
//	if ( aux0 != 0 && aux1 != 0 )
//		ss = ((aux0-48)*10) + (aux1-48);
//
//	//Valor de sensibilidad recibido debe ser entre 1 y 99.
//	if ( ss < 100 && ss > 0 )
//		sens = ss;
//	else
//		ss = sens;
//
//	for ( uint8_t x=0 ; x<= N_VEC ; x++ )
//		{
//			if ( ( (Vlr_Mdd[x] - Vlr_Mdd[y]) < ss && (Vlr_Mdd[x] - Vlr_Mdd[y]) > 0 ) || ( (Vlr_Mdd[y] - Vlr_Mdd[x]) < ss && (Vlr_Mdd[y] - Vlr_Mdd[x]) > 0 ) )
//			{
//				Vlr_Mdd[x] = Vlr_Mdd[y];
//			}
//		}
//}




/*******************************************************************************
 * @fn 		uint8_t busqueda_tramaRX(uint8_t, uint8_t)
 * @details Análisis de recepción de trama
 * @details No Portable
 * @param	uint8_t dato1, dato2
 * @return 	uint8_t: devuelve 1 si la trama es correcta.
*******************************************************************************/
//uint8_t busqueda_tramaRX( uint8_t dato1, uint8_t dato2)
//{
//	uint8_t rx_ok = 0;
//
//	for ( int i = 0 ; i<= MAX_RX ; i++ )
//	{
//		if ( bufferRX[i] == dato1 && bufferRX[i+1] == dato2 )
//		{
//			rx_ok = 1;
//			i = MAX_RX;
//		}
//		else rx_ok = 0;
//	}
//	return rx_ok;
//}




/*******************************************************************************
 * @fn 		uint8_t busqueda_datoRX(uint8_t)
 * @details Análisis de recepción de dato
 * @details No Portable
 * @param	uint8_t dato1
 * @return 	uint8_t: devuelve 1 si el dato se encontró.
*******************************************************************************/
//uint8_t busqueda_datoRX( uint8_t dato1 )
//{
//	uint8_t rx_ok = 0;
//
//	for ( int i = 0 ; i<= MAX_RX ; i++ )
//	{
//		if ( bufferRX[i] == dato1 )
//		{
//			rx_ok = 1;
//			i = MAX_RX;
//		}
//		else rx_ok = 0;
//	}
//	return rx_ok;
//}
