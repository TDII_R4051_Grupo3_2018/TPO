/*******************************************************************************************************************************//**
 *
 * @file		PR_lcd.c
 * @brief		Primitivas de LCD LPC1769
 * @date		02-12-16
 * @author		Matías Grüner
 *
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** INCLUDES
 **********************************************************************************************************************************/
#include "PR_lcd.h"

/***********************************************************************************************************************************
 *** DEFINES PRIVADOS AL MODULO
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** MACROS PRIVADAS AL MODULO
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** TIPOS DE DATOS PRIVADOS AL MODULO
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** TABLAS PRIVADAS AL MODULO
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** VARIABLES GLOBALES PUBLICAS
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** VARIABLES GLOBALES PRIVADAS AL MODULO
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** PROTOTIPO DE FUNCIONES PRIVADAS AL MODULO
 **********************************************************************************************************************************/

 /***********************************************************************************************************************************
 *** FUNCIONES PRIVADAS AL MODULO
 **********************************************************************************************************************************/

 /***********************************************************************************************************************************
 *** FUNCIONES GLOBALES AL MODULO
 **********************************************************************************************************************************/

void Display_LCD( char * msg, char r , char p )
{
	unsigned char i ;

	switch ( r )
	{
		case RENGLON_1:
			Push_LCD( p + 0x80 , LCD_CONTROL );
			break;
		case RENGLON_2:
			Push_LCD( p + 0xc0 , LCD_CONTROL );
			break;
	}
	for( i = 0 ; msg[ i ] != '\0' ; i++ )
		Push_LCD( msg [ i ] , LCD_DATA );
}
