/*
 * @file		Infotronic.c
 * @brief		Drivers de PWM LPC1769
 * @date		14-05-16
 * @author		Matías Grüner
 *
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** INCLUDES
 **********************************************************************************************************************************/
#include "PR_pwm.h"

/***********************************************************************************************************************************
 *** DEFINES PRIVADOS AL MODULO
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** MACROS PRIVADAS AL MODULO
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** TIPOS DE DATOS PRIVADOS AL MODULO
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** TABLAS PRIVADAS AL MODULO
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** VARIABLES GLOBALES PUBLICAS
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** VARIABLES GLOBALES PRIVADAS AL MODULO
 **********************************************************************************************************************************/
static volatile PWM_estado estadoAnt[servos] = {servo_OFF,servo_OFF};

/***********************************************************************************************************************************
 *** PROTOTIPO DE FUNCIONES PRIVADAS AL MODULO
 **********************************************************************************************************************************/

 /***********************************************************************************************************************************
 *** FUNCIONES PRIVADAS AL MODULO
 **********************************************************************************************************************************/

 /***********************************************************************************************************************************
 *** FUNCIONES GLOBALES AL MODULO
 **********************************************************************************************************************************/

/*******************************************************************************
 * @fn			PWM_On();
 * @brief		Enciende el servo que se le pase como parámetro
 * 				al sentido de giro deseado. Los cambios se actualizan al
 * 				final del ciclo
 * @param[in]	outPut			Es el servo (vertical o horizontal):
 * 								-servo_v
 * 								-servo_h
 * @param[in]	sentido_giro	Es justamente el sentido de giro al que se moverá el servo
 * @return		void
 ******************************************************************************/
void PWM_On (PWM_servos outPut, uint32_t sentido_giro)
{
	switch(outPut)
	{
		case servo_v:
			PWM1LER |= (1 << LER2_EN); //(0x01 << 2);
			PWM1MR2 = sentido_giro;
			break;
		case servo_h:
			PWM1LER |= (1 << LER3_EN); //(0x01 << 3);
			PWM1MR3 = sentido_giro;
			break;
		default:
			break;
	}

	if(outPut < servos)
	{
		estadoAnt[outPut] = servo_ON;
	}
}


/*******************************************************************************
 * @fn			PWM_Off();
 * @brief		Apaga el servo que se le pasa como parámetro.Los cambios se
 * 				actualizan al final del ciclo
 * @param[in]	outPut		Es el servo (vertical o horizontal):
 * 							-servo_v
 * 							-servo_h
 * @return		void
 ******************************************************************************/
void PWM_Off (PWM_servos outPut)
{
	switch(outPut)
	{
		case servo_v:
			PWM1MR2 = 1;
			PWM1LER |= (0x01 << LER2_EN);
			break;
		case servo_h:
			PWM1MR3 = 1;
			PWM1LER |= (0x01 << LER3_EN);
			break;
		default:
			break;
	}

	if(outPut < servos)
	{
		estadoAnt[outPut] = servo_OFF;
	}
}


/*******************************************************************************
 * @fn			PWM_Toggle();
 * @brief		Togglea el servo que se pase como parametro al sentido de giro
 * 				deseado. Los cambios se actualizan al
 * 				final del ciclo
 * @param[in]	outPut			Es el servo (vertical o horizontal):
 * 								-servo_v
 * 								-servo_h
 * @param[in]	sentido_giro	Es justamente el sentido de giro al que se moverá el servo
 * @return		void
 ******************************************************************************/
void PWM_Toggle (PWM_servos outPut, uint32_t sentido_giro)
{
	if(outPut < servos)
	{
		if(estadoAnt[outPut] == servo_OFF)
		{
			PWM_On(outPut,sentido_giro);
		}
		else
		{
			PWM_Off(outPut);
		}
	}
}


/*******************************************************************************
 * @fn			PWM_SetEstado();
 * @brief		Setea el estado (ON/OFF) del servo que se le pase como
 * 				parametro al sentido de giro deseado.
 * 				Los cambios se actualizan al final del ciclo
 * @param[in]	outPut			Es el servo (vertical o horizontal):
 * 								-servo_v
 * 								-servo_h
 * @param[in]	sentido_giro	Es justamente el sentido de giro al que se moverá el servo
 * @param[in]	estado 			Estado del servo:
 * 								-servo_ON
 * 								-servo_OFF
 * @return		void
 ******************************************************************************/
void PWM_SetEstado (PWM_servos outPut, uint32_t sentido_giro, PWM_estado estado)
{
	if(estado == ON)
	{
		PWM_On(outPut,sentido_giro);
	}
	else
	{
		PWM_Off(outPut);
	}
}


/*******************************************************************************
 * @fn			PWM1_IRQHandler();
 * @brief		Es la funcion de interrupcion del PWM. En este caso
 * 				se llama cuando interrumpe MATCH0 que es justamente
 * 				al final del ciclo
 * @return		void
 ******************************************************************************/
void PWM1_IRQHandler (void)
{

}




