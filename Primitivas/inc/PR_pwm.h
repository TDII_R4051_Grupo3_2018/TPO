/*
 * @file		DR_PWM.h
 * @brief		Declaracion de registros de PWM
 * @date		04/05/2016
 * @author		Matías Grüner
 */

#ifndef PR_PWM_H_
#define PR_PWM_H_

/***********************************************************************************************************************************
 *** INCLUDES GLOBALES
 **********************************************************************************************************************************/
#include "DR_pwm.h"

/***********************************************************************************************************************************
 *** DEFINES GLOBALES
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** MACROS GLOBALES
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** TIPO DE DATOS GLOBALES
 **********************************************************************************************************************************/
typedef enum {
	servo_v,
	servo_h,
	servos
}PWM_servos;

typedef enum {
	servo_ON,
	servo_OFF
}PWM_estado;

/***********************************************************************************************************************************
 *** VARIABLES GLOBALES
 **********************************************************************************************************************************/

/***********************************************************************************************************************************
 *** PROTOTIPOS DE FUNCIONES GLOBALES
 **********************************************************************************************************************************/
void PWM_On (PWM_servos outPut, uint32_t sentido_giro);
void PWM_Off (PWM_servos outPut);
void PWM_Toggle (PWM_servos outPut, uint32_t sentido_giro);
void PWM_SetEstado (PWM_servos outPut, uint32_t sentido_giro, PWM_estado estado);

#endif /* PR_PWM_H_ */
